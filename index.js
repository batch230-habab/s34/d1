// load the expressjs module into our application and save it in a variable called express
const express = require("express");
const port = 4000;

//app is our server
//create an application that uses express and stores it as app
const app = express();

//middleware (request handlers)
//express.json() is a method which allow us to handle the streaming of data and automatically parse the incoming JSON from our req.body
app.use(express.json());

//mock data
let users = [
    {
        username: "TStark3000",
        email: "starkindustries@mail.com",
        password: "notPeterParker"
    },
    {
        username: "ThorThunder",
        email: "thorStrongestAvenger@mail.com",
        password: "iLoveStormBreaker"
    }
]
let items = [
    {
        name: "Mjolnir",
        price: 50000,
        isActive: true
    },
    {
        name: "Vibranium Shield",
        price: 70000,
        isActive: true
    }
]

app.get("/", (request, response) => {
	response.send("Hello from my first ExpressJSAPI");
	}
)
app.get("/greeting", (request, response) =>{
    response.send("Hello from Batch230-habab");
})
//retrieval of users in mock database
app.get("/users", (req, res)=>{
    res.send(users);
})
//post
app.post("/users" , (req, res) => {
    let newUser = {
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    }
    users.push(newUser);
    console.log(users);
    res.send(users);
})
//put
app.put("/users/:index", (req, res) => {
    console.log(req.body);

    console.log(req.params);
    let index = parseInt(req.params.index); //parseInt because index:"1" is string. converted to integer

    users[index].password = req.body.password;

    res.send(users[index]);
})

//delete the last element in an array
app.delete('/users', (req, res) => {
    users.pop();
    res.send(users);
})


/* let items = [
    {
        name: "Mjolnir",
        price: 50000,
        isActive: true
    },
    {
        name: "Vibranium Shield",
        price: 70000,
        isActive: true
    }
] */

// Activity
/* 
    >>create a new collection in postman called s34-activity
    >>save the postman collection in your s34 folder
*/
// >>create a new route to get and send items array in the client (GET ALL ITEMS)

//[get]
// >> create a new route to get and send items array in the client( get all items)
app.get("/items", (req, res)=>{
    res.send(items);
})

//[post]
// >> create a new route to create and add a new item object in the items array (create ITEM)
    // >> send the updated items array in the client
    // >> check if the post method route for our users for reference
app.post("/items", (req, res) =>{
    let newItem = {
        name: req.body.name,
        price: req.body.price,
        isActive: req.body.isActive
    }
    items.push(newItem);
    console.log(items);
    res.send(items);
})


//[put]
// >> create a new route which can update the price of a single item in the array (update item)
/* 
    >> pass the index number of the item that you want to update in the request params(include an index number to the URL)
    >> add the price update in the request body
    >>reassign the new price from our request body
    >>send the updated item to the client
*/
app.put("/items/:index", (req, res) => {
    console.log(req.body);

    console.log(req.params);
    let index = parseInt(req.params.index);

    items[index].price = req.body.price;

    res.send(items[index]);
})
app.listen(port, () => console.log(`Server is now running at port ${port}`));

